package cz.etn.etnshop.dao;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "product")
public class Product implements Serializable {

	private static final long serialVersionUID = -2739622030641073946L;

	private int id;

	// validation annotations
	@NotEmpty (message = "Please enter a product name.")
	@Size(min=3, max=250, message = "Size must be between 3 - 250 characters")
	private String name;

	// validation annotations
	@NotEmpty (message = "Please enter a serial number.")
	@Size(min=5, max=250, message = "Size must be between 5 - 250 characters")
	private String serialNumber;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// Product name
	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	// Serial number
	@Column(name = "serialNumber", nullable = false)
	public String getSerialNumber() { return serialNumber; }
	public void setSerialNumber(String number) { this.serialNumber = number; }

}
