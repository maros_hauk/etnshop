package cz.etn.etnshop.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {

	@SuppressWarnings("unchecked")
	public List<Product> getProducts(String text) {
		Criteria criteria = getSession().createCriteria(Product.class);

		// add criteria for text search
		if (text != null) {
			criteria.add(Restrictions.or(
					Restrictions.ilike("name", text, MatchMode.ANYWHERE),
					Restrictions.ilike("serialNumber", text, MatchMode.ANYWHERE)));
		}
		return (List<Product>) criteria.list();
	}

	@Override
	public void createProduct(Product product) {
		getSession().save(product);
	}

	@Override
	public void updateProduct(Product product) {
		getSession().update(product);
	}

	@Override
	public Product findProduct(int Id) {
		return (Product)getSession().get(Product.class, Id);
	}

}
