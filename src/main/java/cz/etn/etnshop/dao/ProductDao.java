package cz.etn.etnshop.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface ProductDao {

	@Transactional(readOnly = true)
	List<Product> getProducts(String text);

	@Transactional(readOnly = false)
	void createProduct(Product product);

	@Transactional(readOnly = false)
	void updateProduct(Product product);

	@Transactional(readOnly = true)
	Product findProduct(int Id);

}
