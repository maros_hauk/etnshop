package cz.etn.etnshop.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import cz.etn.etnshop.dao.Product;

public interface ProductService {

	@Transactional(readOnly = true)
	List<Product> getProducts(String text);

	@Transactional(readOnly = false)
	void createProduct(Product product);

	@Transactional(readOnly = false)
	void updateProduct(Product product);

	@Transactional(readOnly = true)
	Product findProduct(int Id);

}
