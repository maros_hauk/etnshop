package cz.etn.etnshop.controller;

import cz.etn.etnshop.dao.Product;
import cz.etn.etnshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;


@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	// list all products
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam (value = "text", required = false) String text) {
		ModelAndView modelAndView = new ModelAndView("product/list");
		modelAndView.addObject("products", productService.getProducts(text));
		return modelAndView;
	}

	// show form to register a product
	@RequestMapping("/register")
	public ModelAndView register() {
		return new ModelAndView("product/register","product", new Product());
	}

	// method to insert a product into the database
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(@Valid Product product, BindingResult result) {
		if (result.hasErrors())	{
			return "product/register";
		}
		else {
			productService.createProduct(product);
			return ("redirect:/product/list");
		}
	}

	// show form to edit a product based on its id
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam("id") int userId) {
		Product product = productService.findProduct(userId);
		ModelAndView modelAndView = new ModelAndView("product/edit", "product", product);
		modelAndView.addObject("id", product.getId());
		modelAndView.addObject("name", product.getName());
		modelAndView.addObject("serialNumber", product.getSerialNumber());
		return modelAndView;
	}

	// method to update a product in the database
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(@Valid Product product, BindingResult result) {
		if (result.hasErrors())	{
			return "product/edit";
		}
		else {
			productService.updateProduct(product);
			return ("redirect:/product/list");
		}
	}

	// search form
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView search() {
		return new ModelAndView("product/search");
	}

}
