<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>etnShop</title>

<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<body>
<div class="container">
	<h2>Update existing product</h2>
    <form:form method="POST" action="/etnshop/product/update" modelAttribute="product">
        <p>
            <form:label path="id">ID</form:label>
            <form:input path="id" value="${id}" class="form-control" readonly="true"/>
        </p>

        <p>
            <form:label path="name">Product Name</form:label>
            <form:input path="name" value="${name}" class="form-control" />
            <form:errors path="name" />
        </p>

        <p>
            <form:label path="serialNumber">Serial Number</form:label>
	        <form:input path="serialNumber" value="${serialNumber}" class="form-control" />
	        <form:errors path="serialNumber" />
        </p>

        <input type="submit" value="Update Product" />

	</form:form>

	<hr>
	<p>
		<a class="btn btn-primary btn-lg" href="/etnshop" role="button">Back to homepage</a>
	</p>
	<footer>
		<p>&copy; Etnetera a.s. 2015</p>
	</footer>
</div>

<spring:url value="/resources/core/css/bootstrap.min.js"
	var="bootstrapJs" />

<script src="${bootstrapJs}"></script>
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>